"use strict";

function Thermometer(source, listeners) {

    var _listeners = listeners;
    var _source = source;

    function start() {
        var reading = _source.read();
        while (reading) {
            _listeners.forEach(function (listener) {
                listener(this);
            }, reading);
            reading = _source.read();
        }
    }

    return {
        start: start
    };
}

function InputSource(temperatureList) {

    var i = 0;
    var list = temperatureList;
    function read() {
        return list[i++];
    }

    return {
        read: read
    };
}

function Temperature(val) {
    this._value = val;
}

Temperature.prototype._convertToCelsius = function () {
    throw new Error("not implemented");
};

Temperature.prototype.toString = function () {
    return this._value;
};

Temperature.prototype.eq = function (other) {
    return this._convertToCelsius()._value == other._convertToCelsius()._value;
};

Temperature.prototype.gt = function (other) {
    return this._convertToCelsius()._value > other._convertToCelsius()._value;
};

Temperature.prototype.lt = function (other) {
    return this._convertToCelsius()._value < other._convertToCelsius()._value;
};

Temperature.prototype.ge = function (other) {
    return this._convertToCelsius()._value >= other._convertToCelsius()._value;
};

Temperature.prototype.le = function (other) {
    return this._convertToCelsius()._value <= other._convertToCelsius()._value;
};

Temperature.prototype.add = function (other) {
    var t1 = this._convertToCelsius();
    var t2 = other._convertToCelsius();
    t1._value += t2._value;
    return t1;
};

Temperature.prototype.sub = function (other) {
    var t1 = this._convertToCelsius();
    var t2 = other._convertToCelsius();
    t1._value -= t2._value;
    return t1;
};

var Celsius = function () {
    Temperature.apply(this, arguments);
};

Celsius.prototype = new Temperature();
Celsius.constructor = Celsius;

Celsius.prototype._convertToCelsius = function () {
    return new Celsius(this._value);
};


function Threshold() {

    function evaluate(reading) {
        throw new Error("not implemented");
    }

    return {
        evaluate: evaluate
    };
}

function ExactTemperatureThreshold(threshold) {
    this._threshold = threshold;
}

ExactTemperatureThreshold.prototype = new Threshold();
ExactTemperatureThreshold.constructor = ExactTemperatureThreshold;

ExactTemperatureThreshold.prototype.evaluate = function (reading) {
    return this._threshold.eq(reading);
};

function DirectionalWithToleranceThreshold(threshold, direction, fluctuation) {
    this._upperThreshold = threshold.add(fluctuation);
    this._lowerThreshold = threshold.sub(fluctuation);
    this._threshold = threshold;
    this._direction = direction;

    this._lastReadings = [];
    this._armed = false;
}

DirectionalWithToleranceThreshold.prototype = new Threshold();
DirectionalWithToleranceThreshold.constructor = DirectionalWithToleranceThreshold;

DirectionalWithToleranceThreshold.prototype.evaluate = function (reading) {

    function isTrendingDown(firstReading, secondReading) {
        return firstReading.gt(secondReading);
    }

    function isTrendingUp(firstReading, secondReading) {
        return firstReading.lt(secondReading);
    }

    function isInsideBoundries(reading, upper, lower) {
        return reading.le(upper) && reading.ge(lower);
    }

    function crossedThreshold(firstReading, secondReading, threshold) {
        return (firstReading.lt(threshold) && secondReading.ge(threshold)) || (firstReading.gt(threshold) && secondReading.le(threshold));
    }

    var lastReading = this._lastReadings[0];
    var upper = this._upperThreshold;
    var lower = this._lowerThreshold;
    var threshold = this._threshold;
    var armed = this._armed;

    var crossed = false;
    
    if (armed) {
        armed = isInsideBoundries(reading, upper, lower);  //check fluctuation, disarm if outside fluctuation boundaries
    } else {
        if (lastReading) {
            crossed = crossedThreshold(lastReading, reading, threshold);
            if (this._direction.toLowerCase() == 'up') {
                crossed = crossed && isTrendingUp(lastReading, reading);
            }
            else if (this._direction.toLowerCase() == 'down') {
                crossed = crossed && isTrendingDown(lastReading, reading);
            }
        } else {
            crossed = threshold.eq(reading);
        }
        armed = crossed;    //arm if crossed
    }

    //save state
    if (this._lastReadings.unshift(reading) > 2) this._lastReadings.pop();
    this._armed = armed;
    return crossed;
};

var source = InputSource([
    new Celsius(-5.5),
    new Celsius(-4.5),
    new Celsius(0),
    new Celsius(-1.3),
    new Celsius(-1.4),
    new Celsius(-1.7),
    new Celsius(3),
    new Celsius(3.1),
    new Celsius(3.2),
    new Celsius(2.9),
    new Celsius(9),
    new Celsius(10.1),
    new Celsius(8.9),
    new Celsius(10.1),
    new Celsius(9.9)
]);

var freezingThreshold = new ExactTemperatureThreshold(new Celsius(0));
var threeUpThreshold = new DirectionalWithToleranceThreshold(new Celsius(3), 'up', new Celsius(0.5));
var oneHalfDownthreshold = new DirectionalWithToleranceThreshold(new Celsius(-1.5), 'down', new Celsius(0.1));
var tenAnyThreshold = new DirectionalWithToleranceThreshold(new Celsius(10), 'any', new Celsius(1));


var listeners = [
    function (reading) {
        console.log('read ', reading);
    },
    function (reading) {
        if (freezingThreshold.evaluate(reading)) {
            console.log("reached freezing point");
        }
    },
    function (reading) {
        if (threeUpThreshold.evaluate(reading)) {
            console.log('reached 3C+-0.5 from below');
        }
    },
    function (reading) {
        if (oneHalfDownthreshold.evaluate(reading)) {
            console.log('reached -1.5C+-0.1 from above');
        }
    },
    function (reading) {
        if (tenAnyThreshold.evaluate(reading)) {
            console.log('reached 10C+-1');
        }
    }
];

var thermometer = new Thermometer(source, listeners);

thermometer.start();